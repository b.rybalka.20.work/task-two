WITH tf as (
    (SELECT 2 as role_id, 'GOD' as role_funny_name  from DUAL)
    UNION
    (SELECT 5 as role_id, 'Slave' as role_funny_name  from DUAL)
    UNION
    (SELECT 16 as role_id, 'Wizard' as role_funny_name  from DUAL)
) 
SELECT tf.role_id, tf.role_funny_name, domain_class_role.role_name FROM tf INNER JOIN domain_class_role 
ON domain_class_role.role_id = tf.role_id;
