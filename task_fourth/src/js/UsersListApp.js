angular
    .module('UsersListApp', ['kendo.directives'])
    .controller("MyCtrl", MyCtrl);

function MyCtrl() {
    this.gridOptions = {
        columns: [ { field: "Name"  },
            { field: "Login" },
            { field: "Email" },
            { field: "Date"  }],
        pageable: true,
        sortable : true,
        filterable : true,
        dataSource: {
            pageSize: 5,
            transport: {
                read: function (e) {
                    var m = [{Name: 'MockName', Login:  "Login",  Email: 'MockEmail',  Date : 'Date'},
                        {Name: 'MockName1', Login: "Login1", Email: 'MockEmail1', Date : 'Date1'},
                        {Name: 'MockName2', Login: "Login2", Email: 'MockEmail2', Date : 'Date2'},
                        {Name: 'MockName3', Login: "Login3", Email: 'MockEmail3', Date : 'Date3'},
                        {Name: 'MockName4', Login: "Login4", Email: 'MockEmail4', Date : 'Date4'},
                        {Name: 'MockName5', Login: "Login5", Email: 'MockEmail5', Date : 'Date5'}];
                    e.success(m);
                }
            }
        }
    }
}




