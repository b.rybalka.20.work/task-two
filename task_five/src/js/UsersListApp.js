angular
    .module('UsersListApp', ['ngRoute', 'kendo.directives'])
    .controller("AddCtrl", AddCtrl)
    .controller('ListCtrl', ListCtrl)
    .config(['$routeProvider', route]);


function route ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'templates/list.html',
            controller : ListCtrl
        })
        .when('/list', {
            templateUrl : 'templates/list.html',
            controller : ListCtrl
        })
        .when('/add', {
            templateUrl : 'templates/add.html',
            controller : AddCtrl
        });
}




