function AddCtrl($scope) {

    $scope.addUser = function () {
            $scope.newUser.Id = users.length + 1;
            $scope.newUser.date = new Date();
            users.push($scope.newUser);
            window.location.replace("#!/list");
    };

    $scope.validatorOptions = {
        messages : {
            isUniqueLoginRule : 'Login should be unique',
            isUniqueEmailRule : 'Email should be unique',
            isLengthLessThenFiftyRule : 'Length should be less then 50',
            isLengthLessThenHundredRule : 'Length should be less then 100',
            isAlphaNumericRule : 'Should consist of letters and digits'
        },

        rules : {
            isAlphaNumericRule : function (input) {
                if (input.is('[name=login]') || input.is('[name=username]') ) {
                    return isAlphaNumericStr(input.val());
                }
                return true;
            },

            isLengthLessThenFiftyRule : function (input) {
                if (input.is('[name=login]') || input.is('[name=email]')) {
                    return isStrLengthLessThen(input.val, 50);                }

                return true;
            },

            isLengthLessThenHundredRule : function (input) {
                if (input.is('[name=username]')) {
                    return isStrLengthLessThen(input.val, 100);
                }

                return true;
            },

            isUniqueLoginRule : function (input) {

                if (!input.is('[name=login]')) {
                    return true;
                }

                var usersLogins = users
                    .map(function (x) {
                        return x['login']
                    });

                return usersLogins.indexOf(input.val()) === -1;
            },

            isUniqueEmailRule : function (input) {

                if (!input.is('[name=email]')) {
                    return true;
                }

                var usersEmails = users
                    .map(function (x) {
                        return x['email']
                    });

                return usersEmails.indexOf(input.val()) === -1 ;
            }

        }
    };

    var isStrLengthLessThen = function (str, length) {
        return str.length < length;
    };

    var isAlphaNumericStr = function (str) {

        for (var i = 0; i < str.length; i++) {

            var ch = str.charCodeAt(i);
            var isAlphaNumeric = isAlphaChar(ch) || isNumericChar(ch);
            if (!isAlphaNumeric ) {
                return false;
            }
        }

        return true;
    };

    var isAlphaChar = function (ch) {
        return ch >= 65 &&  ch <= 122;
    };

    var isNumericChar = function (ch) {
        return ch >=48 && ch <= 57;
    }
}



