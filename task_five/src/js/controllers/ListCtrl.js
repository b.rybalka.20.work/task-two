function ListCtrl($scope) {
    $scope.gridOptions = {
        columns: [{
            field: "name",
            title: "Name",
            filterable : {
                operators : {
                    string : {
                        contains : "contains"
                    }
                }
            }
        },{
            field: "email",
            title: "Email",
            filterable : true
        },{
            title: "Login",
            field: "login",
            filterable : {
                operators : {
                    string  : {
                        contains : "Contains"
                    }
                }
            }
        },{
            field: "date",
            title :"date",
            template: "#= kendo.toString(date, 'MM/dd/yyyy hh:mm') #",
            filterable : {
                extra: true,
                operators : {
                    date : {
                        lt : 'To',
                        gt : 'From'
                    }
                }
            }
        },{
            command : ['edit']
        }],
        editable: "popup",
        filterable : {
            extra : false
        },
        dataSource : {
            data : users,
            schema : {
                model : {
                    id : 'Id',
                    fields : {
                        name : {

                        },
                        email : {

                        },
                        login : {

                        },
                        date : {
                            type : "date",
                            editable : false
                        }
                    }
                }
            }
        },
    }
}