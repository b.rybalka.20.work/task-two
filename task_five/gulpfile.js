var gulp = require('gulp');
var gls = require('gulp-live-server');

var config = {

    distCss: 'dist/css',
    distJs: 'dist/js',
    distHtml: 'dist/',
    distTemplates: 'dist/templates',

    cssDependencies : [
      'node_modules/bootstrap/dist/css/bootstrap.css',
      'node_modules/bootstrap/dist/css/bootstrap-grid.css'
    ],

    jsDependencies : [
        'node_modules/angular-route/angular-route.js',
        'node_modules/angular/angular.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/tether/dist/js/tether.js',
        'src/js/*.js',
        'src/js/**/*Ctrl.js',
    ],

    templates : [
        'src/templates/**/*.html'
    ]
};

gulp.task('build', function () {
    gulp
        .src(config.jsDependencies)
        .pipe(gulp.dest(config.distJs));

    gulp
        .src(config.cssDependencies)
        .pipe(gulp.dest(config.distCss));


    gulp
        .src(config.templates)
        .pipe(gulp.dest(config.distTemplates));

    gulp.src('src/index.html')
        .pipe(gulp.dest(config.distHtml));
});

gulp.task('serve', function () {
    var server = gls.static('dist', 9091);

    server.start();

    gulp.watch('src/**/*', function () {
        gulp.run('build');
    })

});
