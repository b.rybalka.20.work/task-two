var gulp = require('gulp');
var gls = require('gulp-live-server');

var dependencies = {
    cssDependencies : ['node_modules/bootstrap/dist/css/bootstrap.css',
                       'src/css/style.css',
                       'kendoUI/styles/kendo.common.core.min.css',
                       'kendoUI/styles/kendo.rtl.min.css',
                       'kendoUI/styles/kendo.silver.min.css'
                      ],

    jsDependencies  : ['node_modules/jquery/dist/jquery.js',
                       'node_modules/angular/angular.js',
                       'kendoUI/js/angular.min.js',
                       'kendoUI/js/kendo.all.min.js',
                       'src/js/myAngular.js'
                      ]
};

gulp.task('build', function () {

    gulp.src(dependencies.cssDependencies)
        .pipe(gulp.dest('dist/css'));

    gulp.src(dependencies.jsDependencies)
        .pipe(gulp.dest('dist/js'));

    gulp.src('src/index.html')
        .pipe(gulp.dest('dist/'));

});

gulp.task('serve', function () {
    var server = gls.static('dist', 8084);

    server.start();

    gulp.watch('src/*.html', function (file) {
        console.log(file.path);
        gulp.src(file.path)
            .pipe(gulp.dest('dist/'));
    });

    gulp.watch('src/css/*.css', function (file) {
        console.log(file.path);
        gulp.src(file.path)
            .pipe(gulp.dest('dist/css'));
    });

    gulp.watch('src/js/*.js', function (file) {
        console.log(file.path);
        gulp.src(file.path)
            .pipe(gulp.dest('dist/js'));
    });

});


