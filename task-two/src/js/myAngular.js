angular
    .module("kendoExample", [ "kendo.directives" ])
    .controller("MyCtrl", MyCtrl);



function MyCtrl() {

    var self = this;

    self.data = '';

    var foo = function (text) {
        if (self.data) {
            self.data = '';
        } else {
            self.data = text;
        }
    };

    self.helloWorld = function () {
        foo('Hello World!!!');
    };

    self.goodbyeWorld = function () {
        foo('GoodBye World!!!');
    };
}