CREATE TABLE papa (
    papa_id numeric not null,
    CONSTRAINT papa_pk PRIMARY KEY (papa_id)
);

CREATE TABLE son 
(
    son_id numeric(10) not null,
    papa_id numeric(10) not null,
    
    data CLOB,
    
    CONSTRAINT son_fk
        FOREIGN KEY (papa_id)
        REFERENCES papa(papa_id)
);

INSERT INTO son(son_id, papa_id, data) VALUES (1, 1, :x);

/* NEW NEW NEW*/

CREATE OR REPLACE FUNCTION generateBigString 
RETURN CLOB IS 
   d CLOB := ''; 
   loop_counter number(4);
BEGIN 
        FOR loop_counter IN 1..4100
        LOOP
            d:= CONCAT(d, dbms_random.string('U', 1) );
        END LOOP;

   RETURN d; 
END; 
/

DECLARE 
   c CLOB; 
BEGIN 
    c := generateBigString(); 
    INSERT INTO son(son_id, papa_id, data) VALUES (1, 1, c);
END; 
/